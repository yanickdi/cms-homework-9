"""
    This module provides our self-made datastrucure: a two-dimensional Matrix.
"""
import copy

class Matrix2D(object):
    """
    This class represents a two-dimensional Matrix.
    """

    def __init__(self, rows, cols, generator=None):
        """Initializes the Matrix object. Values are initialized with None

        :param rows: positive number of rows
        :param cols: positive number of cols
        :param generator: can be a generator object that fills the matrix line by line
        """
        assert rows >= 0; assert cols >= 0
        self.__rows = rows; self.__cols = cols
        self.__matrix = [ [None] * cols  for i in range(rows) ]
        if generator:
            self.fill_from_list(generator)

    @property
    def rows(self):
        return self.__rows

    @property
    def cols(self):
        return self.__cols

    def fill_from_list(self, iterable):
        """Fills the matrix row by row from a given `iterable`"""
        for i, val in enumerate(iterable):
            row = i // self.cols
            col = i % self.cols
            self.__matrix[row][col] = val

    def enumerate_all(self):
        """like the python built-in enumerate() function, but enumerates over all elements in the matrix, line by line"""
        pos = 0
        for row in self.__matrix:
            for col in row:
                yield pos, col
                pos += 1

    def copy(self):
        """Returns a new Matrix2D Object and copies every element to the new object (doesn't do a deep copy)"""
        newobj = Matrix2D(self.rows, self.cols)
        for m, row in enumerate(self.__matrix):
            for n, col in enumerate(row):
                newobj.__matrix[m][n] = copy.copy(col)
        return newobj

    def __getitem__(self, key):
        """Operator [key]  : Gets a new MatrixRow Object"""
        if key >= self.rows or key < 0:
            raise IndexError('Row out of range')
        return Matrix2D.MatrixRow(self, key)

    def __add__(self, other):
        """Performs a matrix addition and returns the result"""
        if self.rows != other.rows or self.cols != other.cols:
            raise ValueError('Matrices have to have the same amount of columns and rows')
        new_m = Matrix2D(self.rows, self.cols)
        for m in range(self.rows):
            for n in range(self.cols):
                new_m.__matrix[m][n] = self.__matrix[m][n] + other.__matrix[m][n]
        return new_m

    def __mul__(self, other):
        """Performs a matrix multiplication"""
        if self.cols != other.rows:
            raise ValueError('Number of columns of the first matrix is unequal to the number of rows of the second matrix, multiplication impossible')
        new_m = Matrix2D(self.rows, other.cols)
        for i in range(new_m.rows):
            for k in range(new_m.cols):
                sum = 0
                for j in range(self.cols):
                    sum += self.__matrix[i][j] * other.__matrix[j][k]
                new_m.__matrix[i][k] = sum
        return new_m

    def get_list(self):
        """Get a one-dimensional list including all elements row by row"""
        return [val for i,val in self.enumerate_all()]

    class MatrixRow(object):
        """Represents a Matrix2D Row"""
        def __init__(self, matrix_obj, key):
            self.__matrix_obj = matrix_obj
            self.__row = key

        def __str__(self):
            vals = ','.join(str(v) for v in self.__matrix_obj._Matrix2D__matrix[self.__row])
            return 'Row[{}]: ({})'.format(self.__row, vals)

        def __getitem__(self, key):
            """Gets a value at column position `key`  like matrixrow[colnum]"""
            rowlist = self.__matrix_obj._Matrix2D__matrix[self.__row]
            if key < 0 or key >= len(rowlist):
                raise IndexError('Column out of range')
            return rowlist[key]

        def __setitem__(self, key, value):
            """Sets a value at column position `key` like matrixrow[4] = 'asd'"""
            rowlist = self.__matrix_obj._Matrix2D__matrix[self.__row]
            if key < 0 or key >= len(rowlist):
                raise IndexError('Column out of range')
            rowlist[key] = value