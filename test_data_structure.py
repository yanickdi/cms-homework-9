"""This module provides a unit test for testing our Data Structure Matrix2D"""

import unittest
from data_structure import Matrix2D

class TestDataStructure(unittest.TestCase):

    def setUp(self):
        pass

    def test_iterate(self):
        """tests if one can iterate over the rows and the columns of the matrix"""
        m = Matrix2D(2,2)
        m[0][0] = 0
        m[0][1] = 1
        m[1][0] = 2
        m[1][1] = 3
        for rownum, row in enumerate(m):
            for colnum, col in enumerate(row):
                position = rownum * m.rows + colnum
                self.assertEqual(position, col, 'values are not on the right position')

    def test_count_element(self):
        """tests if the matrix object holds the right amount of values"""
        ROWS = 30
        COLS = 112
        m = Matrix2D(ROWS, COLS)
        self.assertEqual(m.rows, ROWS)
        self.assertEqual(m.cols, COLS)
        i = 0
        for r in m:
            for c in r:
                i += 1
        self.assertEqual(i, ROWS * COLS)

    def test_from_list(self):
        """tests the from_list() method"""
        ROWS = 2
        COLS = 3
        NUM_ELEMS = ROWS * COLS
        LIST = list(range(NUM_ELEMS))
        m = Matrix2D(ROWS,COLS)
        m.fill_from_list(LIST)
        position = 0
        for r in m:
            for c in r:
                self.assertEqual(LIST[position], c)
                position += 1
        self.assertEqual(position, NUM_ELEMS)

    def test_initialize_from_iterable(self):
        """ tests the __init__ function with a generator initialization"""
        m = Matrix2D(2,3, generator = range(6))
        self.assertEqual(m[1][2], 5)

    def test_enumerate_all(self):
        """tests the enumerate_all() method"""
        values = [i**2 for i in range(6)]
        m = Matrix2D(2,3, generator = values)
        pos = 0
        for i, val in m.enumerate_all():
            self.assertEqual(pos, i)
            self.assertEqual(val, i**2)
            pos += 1

    def test_copy(self):
        """tests the copy method"""
        m = Matrix2D(5,5, range(5*5))
        m2 = m.copy()
        self.assertEqual(m.rows, m2.rows)
        self.assertEqual(m.cols, m2.cols)
        for rownum in range(m.rows):
            for colnum in range(m.cols):
                #check if the same
                self.assertEqual(m[rownum][colnum], m2[rownum][colnum])
                #alter the first
                m[rownum][colnum] += 2
                #check if the same
                self.assertNotEqual(m[rownum][colnum], m2[rownum][colnum])

    def test_matrix_plus(self):
        """tests if the plus operator works"""
        m1 = Matrix2D(2,3, range(6))
        m2 = Matrix2D(2,3, range(6))
        for i, val in (m1+m2).enumerate_all():
            self.assertEqual(val, i*2)

    def test_multiply(self):
        """tests the multiply operator"""
        m1 = Matrix2D(2,3, [3,2,1,  1,0,2])
        m2 = Matrix2D(3,2, [1,2,  0,1,  4,0])
        RESULT_ROWS = 2
        RESULT_COLS = 2
        RESULT_LIST = [7,8,  9, 2]
        result = m1 * m2
        self.assertEqual(result.rows, RESULT_ROWS)
        self.assertEqual(result.cols, RESULT_COLS)
        self.assertListEqual(result.get_list(), RESULT_LIST)


if __name__ == '__main__':
    unittest.main()